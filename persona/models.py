from django.db import models

class DocumentType(models.Model):
    name = models.CharField(max_length=10)
    
    def __str__(self):
        return f'{self.name}'

class Sex(models.Model):
    name = models.CharField(max_length=10)
 
    def __str__(self):
        return f'{self.name}'
    
class CivilStatus(models.Model):
    name = models.CharField(max_length=10)
    
    def __str__(self):
        return f'{self.name}'
    
class Province(models.Model):
    name = models.CharField(max_length=255)
    
    def __str__(self):
        return f'{self.name}'

class Person(models.Model):
    name = models.CharField(max_length=255)
    surname = models.CharField(max_length=255)
    age = models.IntegerField()
    sex = models.ForeignKey(Sex, on_delete=models.SET_NULL, null=True)
    document_type = models.ForeignKey(DocumentType, on_delete=models.SET_NULL, null=True)
    civil_status = models.ForeignKey(CivilStatus, on_delete=models.SET_NULL, null=True)
    province = models.ForeignKey(Province, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return f'{self.name} {self.surname}'


    