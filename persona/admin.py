from django.contrib import admin
from .models import *

admin.site.register(Person)
admin.site.register(Sex)
admin.site.register(DocumentType)
admin.site.register(CivilStatus)
admin.site.register(Province)