from django.shortcuts import redirect, render
from .models import *

#Crud Person
def list_person(request):
    persons = Person.objects.all()
    sexs = Sex.objects.all()
    document_types = DocumentType.objects.all()  
    civil_status = CivilStatus.objects.all()  
    provinces = Province.objects.all()
    return render(request, 'person.html', {'persons':persons, 'sexs':sexs, 'document_types':document_types, 'civil_status_collection':civil_status, 'provinces':provinces})

def create_person(request):
    name = request.POST["name"]
    surname = request.POST["surname"]
    age = request.POST["age"]
    sex_id = request.POST["sex"]
    sex = Sex.objects.get(id=sex_id)
    document_type_id = request.POST["document_type"]
    document_type = DocumentType.objects.get(id=document_type_id)
    civil_status_id = request.POST["civil_status"]
    civil_status = CivilStatus.objects.get(id=civil_status_id)
    province_id = request.POST["province"]
    province = Province.objects.get(id=province_id)  
    person = Person(name=name, surname=surname, age=age, sex=sex, document_type=document_type, civil_status=civil_status, province=province)
    person.save()
    return redirect('/persona/')

def update_person(request, id):
    person = Person.objects.get(id=id)
    name = request.POST["name"]
    surname = request.POST["surname"]
    age = request.POST["age"]
    sex_id = request.POST["sex"]
    sex = Sex.objects.get(id=sex_id)
    document_type_id = request.POST["document_type"]
    document_type = DocumentType.objects.get(id=document_type_id)
    civil_status_id = request.POST["civil_status"]
    civil_status = CivilStatus.objects.get(id=civil_status_id)
    province_id = request.POST["province"]
    province = Province.objects.get(id=province_id)
    person.name = name
    person.surname = surname
    person.age = age
    person.sex = sex
    person.document_type = document_type
    person.civil_status = civil_status
    person.province = province
    person.save()
    return redirect('/persona/')

def delete_person(request, id):
    person = Person.objects.get(id=id)
    person.delete()
    return redirect('/persona/')

    
