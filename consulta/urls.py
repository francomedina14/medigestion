from django.urls import path
from .views import *

urlpatterns = [
    path('', list_inquiries),
    path('crear/', create_inquiry, name="create_inquiry"),
]