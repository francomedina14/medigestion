# Generated by Django 4.0.2 on 2022-05-02 23:15

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('persona', '0007_province_alter_person_province'),
        ('usuario', '0004_delete_location_delete_province'),
        ('consulta', '0002_remove_consultingroom_location_consultingroom_active_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='consultingroom',
            name='city',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='usuario.city'),
        ),
        migrations.AlterField(
            model_name='inquiry',
            name='person',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='persona.person'),
        ),
    ]
