from operator import mod
from django.db import models
from usuario.models import City
from persona.models import Person

class ConsultingRoom(models.Model):
    name = models.CharField(max_length=255)
    city = models.ForeignKey(City, on_delete=models.CASCADE, null=False)
    active = models.BooleanField(default=True)

    def __str__(self):
        return f'{self.name}'

class Inquiry(models.Model):
    person = models.ForeignKey(Person, on_delete=models.CASCADE, null=False)
    consulting_room = models.ForeignKey(ConsultingRoom, on_delete=models.CASCADE, null=False)
    active = models.BooleanField(default=True)

    def __str__(self):
        return f'{self.person}'
        

