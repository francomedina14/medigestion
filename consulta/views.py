from django.shortcuts import redirect, render
from .models import *
from persona.models import Person

def list_inquiries(request):
    inquiries = Inquiry.objects.all()
    persons = Person.objects.all()
    consulting_rooms = ConsultingRoom.objects.all()
    return render(request, 'inquiry.html', {"inquiries":inquiries, "persons":persons, "consulting_rooms":consulting_rooms})

def create_inquiry(request):
    person_id = request.POST["person_id"]
    person = Person.objects.get(id=person_id)
    consulting_room_id = request.POST("consulting_room_id")
    consulting_room = ConsultingRoom.objects.get(id=consulting_room_id)
    inquiry = Inquiry(person=person, consulting_room=consulting_room)
    inquiry.save()
    return redirect('/consulta/')
    
def finilize_inquiry(request, id):
    inquiry = Inquiry.objects.get(id=id)
    inquiry.active = False
    inquiry.save()
    return redirect('/consulta/')



