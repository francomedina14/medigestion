from django.shortcuts import render, redirect
from .models import *
from usuario.models import Province

# Crud de location.
def list_locations(request):
    location = Location.objects.all()
    return render(request, 'index.html', {'locations':location})

def create_locations(request):
    name = request.POST["name"]
    locations = Location(name=name)
    locations.save()
    return redirect('/')

def update_locations(request, id):
    locations = Location.objects.get(id=id)
    name = request.POST["name"]
    locations.name = name
    locations.save()
    return redirect('/')

def delete_locations(request, id) :
    locations = Location.objects.get(id=id)
    locations.delete()
    return redirect('/')

# Crud de City
def list_cities(request):
    cities = City.objects.all()
    return render(request, 'index.html', {'cities':cities})

def create_cities(request):
    name = request.POST["name"]
    cities = City(name=name)
    cities.save()
    return redirect('/')

def update_civilStatus(request, id):
    city= City.objects.get(id=id)
    name = request.POST["name"]
    city.name = name
    city.save()
    return redirect('/')

def delete_city(request, id):
    city = City.objects.get(id=id)
    city.delete()
    return redirect('/') 