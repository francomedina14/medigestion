//INDICACIONES INICIALES

UNA VEZ CLONADO EL PROYECTO SE DEBEN SEGUIR ESTOS PASOS:

1) Dentro de la carpeta raiz (medigestion), crear el entorno virtual:

pip install virtualenv

2) Generar entorno virtual:

virtualenv venv

3) Activar el entorno virtual:

* WINDOWS

cd venv/scripts/activate

* MAC/LINUX

source venv/bin/activate

4) Posicionarse nuevamente en la carpeta raiz del proyecto e instalar los paquetes necesarios:

pip install -r requirements.txt

5) Una vez completa la instalacion, correr el proyecto:

python manage.py runserver